#!/usr/bin/env python2.7
"""
parse_download_page.py: Parse the phys course download page 
Copyright (C) 2017  Rico Ka Lok Lo

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

from lxml import html
import requests
import re
from optparse import OptionParser
import sqlite3
import hashlib
import os
from datetime import datetime

class download_page_parser(object):
	def __init__(self, course, term, url, database, username, password = ""):
		self.course = course
		self.term = term
		self.url = url
		self.database = database
		self.username = username
		self.password = password

	@staticmethod
	def get_true_url(url, base_url = "http://www.phy.cuhk.edu.hk"):
		if url.startswith(base_url):
			return url
		else:
			return base_url + url

	@staticmethod
	def get_relative_url(url, base_url = "http://www.phy.cuhk.edu.hk"):
		if url.startswith(base_url):
			return url.replace(base_url, "")
		else:
			return url

	def download(self):
		# Download the download page for scrapping
		page_url = download_page_parser.get_true_url(self.url)

		if self.username is not None:
			download_page = requests.get(page_url, auth = (self.username, self.password))
		else:
			download_page = requests.get(page_url)

		xml_tree = html.fromstring(download_page.content)

		# Check whether the auth is done properly
		try:
			if xml_tree.xpath("//title")[0].text == "Authorization Required":
				return False
		except:
			return False
		self.content = download_page.content
		return True

	def save_content(self, root_directory, multiple_version = False):
		store_path = os.path.abspath(download_page_parser.get_true_url(download_page_parser.get_relative_url(self.url), base_url = root_directory))
		os.system("mkdir -p {0}".format(os.path.dirname(store_path)))
		if multiple_version:
			os.system("mkdir -p {0}/{1}".format(os.path.dirname(store_path), self.compute_hash(self.content)))
			store_path = "{0}/{1}/{2}".format(os.path.dirname(store_path), self.compute_hash(self.content), os.path.basename(store_path))
		file_handler = open(store_path,"w")
		file_handler.write(self.content)

	def compute_hash(self, content):
		# Compute the SHA224 hash
		sha224_hash = hashlib.sha224(content).hexdigest()
		return sha224_hash

	@staticmethod
	def connect_to_database(database):
		# Make a connection to the database, if self.database is actually a string
		if type(database) is str:
			conn = sqlite3.connect(database)
		elif type(database) is sqlite3.Connection:
			conn = database
		else:
			# Unsupported type
			raise TypeError("Unknown type of database object is passed!")
		return conn		

	def write_log_to_database(self):
		conn = download_page_parser.connect_to_database(self.database)
		c = conn.cursor()
		c.execute("INSERT INTO Update_logs(code, term, hash_value, update_time) VALUES ('{0}', '{1}', '{2}', '{3}')".format(self.course.upper(), self.term, self.compute_hash(self.content), datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
		c.close()

"""
Main
"""
if __name__ == "__main__":
	parser = OptionParser()
	parser.add_option("--course", metavar = "CODE", type = "string", help = "The course code of the course to be parsed")
	parser.add_option("--url", metavar = "URL", type = "string", help = "The URL of download page to be parsed")
	parser.add_option("--term", metavar="TERM", type = "string", help = "The term that the course is offered")
	parser.add_option("--database", metavar = "PATH", type = "string", help = "The path of sqlite3 db file to which the course info will be written")
	parser.add_option("--username", metavar = "USERNAME", type = "string", help = "The username for authentication of the download page")
	parser.add_option("--password", metavar = "PASSWORD", type = "string", help = "The password for authentication of the download page")
	parser.add_option("--root-directory", metavar = "DIR", type = "string", help = "The path of the root directory storing the local webpage files")
	(options, args) = parser.parse_args()

	# Check options
	required_options = ["course", "term", "url", "database", "root-directory"]
	missing_options = [option for option in required_options if getattr(options, option.replace("-", "_")) is None]

	if missing_options != []:
		print("The following option(s) is/are missing:" + ", ".join(missing_options))

	assert missing_options == []

	if options.password is not None and options.username is None:
		raise ValueError("Must provide a username if password is provided!")

	if options.password is None:
		options.password = ""

	page_parser = download_page_parser(options.course, options.term, options.url, options.database, options.username, options.password)
	if not page_parser.download():
		raise ValueError("Authorization Required or Failed!")
	page_parser.save_content(options.root_directory, multiple_version = True)
	page_parser.write_log_to_database()
