#!/usr/bin/env python2.7
"""
update_notifier_worker.py: check the course download webpage automatically
Copyright (C) 2017  Rico Ka Lok Lo

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

from optparse import OptionParser
import sqlite3
import threading
from parse_download_page import download_page_parser
from datetime import datetime

"""
Main
"""
base_url = "http://www.phy.cuhk.edu.hk"
download_url = "download/index.html"

parser = OptionParser()
parser.add_option("--term", metavar = "TERM", type = "string", help = "The (current) term that the notifier is monitoring")
parser.add_option("--database", metavar = "PATH", type = "string", help = "The path of sqlite3 db file to which the course info is recorded")
parser.add_option("--interval", metavar = "MINUTE", type = "int", default = 15, help = "The interval (in minute) that the notifier checks the course web pages.")
parser.add_option("--root-directory", metavar = "DIR", type = "string", help = "The path of the root directory storing the local webpage files")
(options, args) = parser.parse_args()

# Check options
required_options = ["term", "database", "root-directory"]
missing_options = [option for option in required_options if getattr(options, option.replace("-","_")) is None]

if missing_options != []:
	print("The following option(s) is/are missing:" + ", ".join(missing_options))

assert missing_options == []

def loop():
	def get_course_list(conn):
		c = conn.cursor()
		c.execute("SELECT code, url, username, password FROM Courses WHERE term = '{0}'".format(options.term))
		course_list = c.fetchall()
		c.close()
		return course_list	
	
	def get_latest_hash(conn, code):
		c = conn.cursor()
		c.execute("SELECT hash_value FROM Update_logs WHERE code = '{0}' ORDER BY update_time DESC LIMIT 1;".format(code))
		if c.fetchone() is None:
			latest_hash = None
		else:
			latest_hash = c.fetchone()[0]
		c.close()
		return latest_hash

	conn = download_page_parser.connect_to_database(options.database)
	course_list = get_course_list(conn)

	for course in course_list:
		page_parser = download_page_parser(course[0], options.term, course[1] + download_url, conn, course[2], course[3])
		if page_parser.download():
			current_hash = page_parser.compute_hash(page_parser.content)
			latest_hash = get_latest_hash(conn, course[0])
			if current_hash != latest_hash:
				# There is something changed in the download page!
				page_parser.save_content(options.root_directory, multiple_version = True)
				page_parser.write_log_to_database()
	
	conn.commit()
	conn.close()
	print("Log: Completed a check at {0}".format(datetime.now()))
	# Restart this loop function after some interval
	threading.Timer(60*options.interval, loop).start()

loop()
