#!/usr/bin/env python2.7
"""
parse_course_offered.py: Parse the courses offered by physics dept as listed on the index page
Copyright (C) 2017  Rico Ka Lok Lo

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

"""

from lxml import html
import requests
import re
from optparse import OptionParser
import sqlite3
import json

parser = OptionParser(description = "Parsing the courses offered by Department of Physics listed on the course web page")
parser.add_option("--database", metavar = "PATH", type = "string", help = "The path of sqlite3 db file to which the parsed course info will be written; if it is not specified, json string will be printed instead")
(options, args) = parser.parse_args()

base_url = "http://www.phy.cuhk.edu.hk"
index_url = "/course/index.php"

# Download the course index page for scrapping
course_webpage = requests.get(base_url + index_url)
xml_tree = html.fromstring(course_webpage.content)

# Analyse how many terms of courses are listed
terms_available = []
a_nodes = xml_tree.xpath("//@name")
for a_node in a_nodes:
	re_search = re.search("[0-9]{4}[ab]", a_node) # Expect sth like 1617b
	if re_search is not None:
		terms_available.append(re_search.group(0))

# Make a list of courses offered in a term
courses_offered = {}
terms_iterator = iter(terms_available)

blockquotes = xml_tree.xpath("//blockquote") # The courses info is listed inside a blockquote node
for blockquote in blockquotes:
	div_nodes = blockquote.xpath("div")
	if len(div_nodes) == 0:
		continue # This blockquote does not contain course info

	courses_offered_this_term = []

	for div_node in div_nodes:
		course = {}
		course_code_re_search = re.search("PHYS [0-9]{4}.", div_node.xpath("a")[0].text)
		if course_code_re_search is None:
			continue # We only analyse PHYS courses
		course_code = course_code_re_search.group(0).replace(" ", "")
		course_title = re.split("PHYS [0-9]{4}.", div_node.xpath("a")[0].text)[1] # Only scrape undergraduate courses (1000-4000 levels)
		course["code"] = course_code
		course["title"] = course_title
		course["url"] = div_node.xpath("a")[0].values()[0] # Store the relative address
		courses_offered_this_term.append(course)

	try:
		courses_offered[terms_iterator.next()] = courses_offered_this_term
	except StopIteration:
		pass

# Write the info to the database
if options.database is not None:
	conn = sqlite3.connect(options.database)
	c = conn.cursor()
	
	for courses_offered_in_a_term in courses_offered.items():
		term = courses_offered_in_a_term[0]
		courses = courses_offered_in_a_term[1]
		for course in courses:
			c.execute("INSERT INTO Courses(code, title, term, url) VALUES ('{0}', '{1}', '{2}', '{3}')".format(course['code'], course['title'], term, course['url']))
	
	conn.commit()
	conn.close()
else:
	# Output as json string
	print(json.dumps(courses_offered))
