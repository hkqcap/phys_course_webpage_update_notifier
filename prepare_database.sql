CREATE TABLE Courses(
	code VARCHAR(9),
	title VARCHAR(255),
	url VARCHAR(1000),
	term CHAR(5),
	username VARCHAR(100),
	password VARCHAR(100),
	PRIMARY KEY (code, term)
);

CREATE TABLE Update_logs(
	code VARCHAR(9),
	term CHAR(5),
	update_time TIMESTAMP,
	hash_value VARCHAR(56),
	PRIMARY KEY (code, term, hash_value),
	FOREIGN KEY (code, term) REFERENCES Courses(code, term)
);
